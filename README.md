# stupidly simple blog!

a simple PHP + SQLite Markdown blog engine!

might make it more complete depending on personal needs and motivation, feedback appreciated.

## requirements
- PHP 7.4 (may or may not work on 8.x yet. this is old code.)
with support for SQLite
- Composer

## install
1. clone the repo
1. set the repo folder as your webroot
1. prevent from accessing the ./private folder
1. install dependencies with `composer install`
1. copy ./example/config.php to ./config.php, 
./example/data.db to ./private/data.db

## credits
- water theme uses [Water.css](https://watercss.kognise.dev/) by [Kognise](https://kognise.dev/)
- uses [Parsedown](https://parsedown.org/) to parse Markdown posts (only for now?)
- structure heavily inspired by OpenClassrooms's [French language PHP MVC course](https://openclassrooms.com/en/courses/4670706-adoptez-une-architecture-mvc-en-php)
- inspiration and motivation taken from [BlogoText](https://blogotext.org/)