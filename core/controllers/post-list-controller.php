<?php
ob_start();

require BASEDIR . "/core/models/posts-model.php";

$perpage = get_option_value("posts_per_page", false);
!isset($perpage) ? $perpage = -1 : $perpage;

$page_count = get_page_count($perpage);

isset($_GET["page"]) && $_GET["page"] > 0 ? $page = $_GET["page"] : $page = 1;

$posts = get_posts($perpage, $page);
require BASEDIR . "/core/views/post-list-view.php";

$content = ob_get_clean();
require TEMPLATEDIR . "/default.php";